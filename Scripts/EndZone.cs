using Godot;
using System;

public class EndZone : ColorRect
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {

    }

    public async void OnArea2DAreaEntered(Area2D area)
    {
        Dice dice = area.GetParent().GetParentOrNull<Dice>();
        if (dice == null) return;

        dice.DeliverySuccessful();
    }
}
