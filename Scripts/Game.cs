using Godot;
using System;

public class Game : Node
{
    public static event Action OnRestartGame;

    private GUI _gui;
    private CardManager _cardManager;
    private MusicManager _musicManager;
    private StateMachine _stateMachine;

    private int _turn = 0;

    public override void _Ready()
    {
        _gui = GetNode<GUI>("/root/Game/GUI");
        _cardManager = GetNode<CardManager>("/root/CardManager");
        _musicManager = GetNode<MusicManager>("/root/MusicManager");
        _stateMachine = GetNode<StateMachine>("/root/StateMachine");

        _musicManager.PlayMusic("background.ogg");

        // Show the menu scene on load.
        AddChild((Menu) GD.Load<PackedScene>("res://Menu.tscn").Instance());
    }

    private void StartNextTurn()
    {
        _turn++;

        if (_turn > 5)
        {
            _stateMachine.GameOver();
            return;
        }

        _gui.UpdateTurn(_turn);
        _stateMachine.StartNewTurn();
    }

    private void RestartGame()
    {
       OnRestartGame?.Invoke();
       CallDeferred(nameof(DoRestartGame));
    }

    private void DoRestartGame()
    {
        // Reset all counters.
        _turn = 0;
        CallDeferred(nameof(StartNextTurn));
    }

    public override void _EnterTree()
    {
        StateMachine.OnTurnFinished += StartNextTurn;
        GameOverPopup.OnPopupDismissed += RestartGame;
        Tutorial.OnTutorialDismissed += StartNextTurn;
    }

    public override void _ExitTree()
    {
        StateMachine.OnTurnFinished -= StartNextTurn;
        GameOverPopup.OnPopupDismissed -= RestartGame;
        Tutorial.OnTutorialDismissed -= StartNextTurn;
    }

}
