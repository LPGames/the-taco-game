using System;
using Godot;

public class GameOverPopup : WindowDialog
{
    public static event Action OnPopupShowing;
    public static event Action OnPopupDismissed;

    private PointsManager _pointsManager;
    private Label _moneyAmount;

    public override void _Ready()
    {
        _pointsManager = GetNode<PointsManager>("/root/PointsManager");
        _moneyAmount = GetNode<Label>("Copy/Money/Amount");
    }


    public void OnGameOverPopupAboutToShow()
    {
        OnPopupShowing?.Invoke();
        int points = _pointsManager.GetPoints();

        _moneyAmount.Text = "$" + points;
    }

    public void OnButtonPressed()
    {
        Hide();
    }

    public void OnGameOverPopupPopupHide()
    {
        OnPopupDismissed?.Invoke();
    }
}
