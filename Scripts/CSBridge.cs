using Godot;
using System;

public class CSBridge : Node
{
    [Signal]
    public delegate void PointsScored(int points);

    [Signal]
    public delegate void PointsFetch();

    public static event Action<string[]> OnHighscoresFetched;

    private void SubmitHighscore(int points)
    {
        EmitSignal(nameof(PointsScored), points);
    }

    private void RequestHighscores()
    {
        EmitSignal(nameof(PointsFetch));
    }

    public void OnPointsFetched(string[] points)
    {
        OnHighscoresFetched?.Invoke(points);
    }

    public override void _EnterTree()
    {
        Highscores.OnHighscoresRequested += RequestHighscores;
        PointsManager.OnSubmitHighscore += SubmitHighscore;
    }

    public override void _ExitTree()
    {
        Highscores.OnHighscoresRequested -= RequestHighscores;
        PointsManager.OnSubmitHighscore -= SubmitHighscore;
    }
}
