using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TacoGame.Scripts.States;

public class DiceController : Node
{
    public static event Action OnDiceAssigned;
    public static event Action OnDiceNotAssigned;

    public static event Action OnAllDiceDelivered;
    public static event Action OnDiceCountChanged;
    public static event Action OnGameOver;

    private StateMachine _stateMachine;
    private PointsManager _pointsManager;
    private SFX _sfx;

    private Vector2 _startPosition;

    private List<Dice> _dice = new List<Dice>();

    private Restaurant[] _restaurants;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GD.Randomize();

        _stateMachine = GetNode<StateMachine>("/root/StateMachine");
        _pointsManager = GetNode<PointsManager>("/root/PointsManager");
        _sfx = GetNode<SFX>("/root/SFX");

        _startPosition = GetNode<Position2D>("Start").GlobalPosition;

        var nodes = GetTree().GetNodesInGroup("restaurant");
        _restaurants = new Restaurant[nodes.Count];
        for (int i = 0; i < nodes.Count; i++)
        {
            _restaurants[i] = nodes[i] as Restaurant;
        }

        OnRestartGame();
    }

    private async Task InitializeDice()
    {
        UpdateDiceCount();
        MoveDiceToStart();
    }

    public void MoveDiceToStart()
    {
        int count = _dice.Count();
        for (int i = 0; i < count; i++)
        {
            if (_dice[i].IsInPlay) continue;

            _dice[i].ResetTurn(_startPosition);
        }

        // Handle removing of dead dice.
        for (int i = 0; i < count; i++)
        {
            if (_dice[i].IsDead())
            {
                DiceDeath(_dice[i], true);
                i--;
                count--;
            }
        }

        foreach (Restaurant restaurant in _restaurants)
        {
            restaurant.Deselect();
        }
    }

    public bool CanMoveDice()
    {
        string[] allowedStates = {
            nameof(PlayerMoveDice),
            nameof(PlayerConfirmDice)
        };

        if (allowedStates.Contains(_stateMachine.GetCurrentState()))
        {
            return true;
        }

        return false;
    }

    public Restaurant GetClosestHolder(Vector2 dicePosition)
    {
        int shortestDistance = 150;
        Restaurant chosenRestaurant = null;
        foreach (var restaurant in _restaurants)
        {
            int distance = (int) dicePosition.DistanceTo(restaurant.GlobalPosition);

            if (distance < shortestDistance)
            {
                chosenRestaurant = restaurant;
                shortestDistance = distance;
            }
        }

        return chosenRestaurant;
    }

    public async void RollDice()
    {
        MoveDiceToStart();
        Tween tween = GetNode<Tween>("Tween");

        for (int i = 0; i < _dice.Count(); i++)
        {
            int value = (int) (GD.Randi() % 6 + 1);
            _dice[i].SetDiceScore(value);

            Vector2 rollPosition = GetChild<Node2D>(i).GlobalPosition;
            _dice[i].SetRollPosition(rollPosition);

            // tween dice to random position.
            tween.InterpolateProperty(_dice[i], "global_position", _dice[i].GlobalPosition,
                rollPosition, 0.7f, Tween.TransitionType.Linear, Tween.EaseType.InOut);
        }

        tween.Start();

        _sfx.PlayDiceSound();

        await ToSignal(tween, "tween_completed");

        for (int i = 0; i < _dice.Count(); i++)
        {
            _dice[i].SetPlacePosition();
        }

    }

    private void ScorePoints()
    {
        for (int i = 0; i < _dice.Count; i++)
        {
            _pointsManager.AddPoints(_dice[i].GetPoints());
        }
    }

    private void StartMovingDice()
    {
        for (int i = 0; i < _dice.Count; i++)
        {
            if (!_dice[i].IsInPlay) continue;

            _dice[i].StartMoving();
        }
    }

    public async void CheckForAllDelivered()
    {
        if (_dice.Count == 0)
        {
            OnGameOver?.Invoke();
            return;
        }

        for (int i = 0; i < _dice.Count; i++)
        {
            if (_dice[i].IsInPlay) return;
        }

        _sfx.PlaySound("glassbell.wav");

        await ToSignal(GetTree().CreateTimer(0.3f), "timeout");

        ScorePoints();
        OnAllDiceDelivered?.Invoke();
    }

    private void OnRestartGame()
    {
        int diceCount = _dice.Count;
        for (int i = 0; i < diceCount; i++)
        {
            _dice[0].QueueFree();
            _dice.Remove(_dice[0]);
        }

        for (int i = 0; i < 4; i++)
        {
            var dice = (PackedScene) GD.Load("res://Prefabs/Dice.tscn");
            var instance = (Dice) dice.Instance();
            instance.SetController(this);
            AddChild(instance);

            _dice.Add(instance);
        }

        CallDeferred(nameof(InitializeDice));
    }

    public override void _EnterTree()
    {
        GUI.OnRollButtonPressed += RollDice;
        PlayGame.OnEnter += StartMovingDice;
        Game.OnRestartGame += OnRestartGame;
    }

    public override void _ExitTree()
    {
        GUI.OnRollButtonPressed -= RollDice;
        PlayGame.OnEnter -= StartMovingDice;
        Game.OnRestartGame -= OnRestartGame;
    }

    public void DiceDeath(Dice dice, bool remove = false)
    {
        if (remove)
        {
            _dice.Remove(dice);
            dice.QueueFree();
        }
        CheckForAllDelivered();
        UpdateDiceCount();
    }

    private void UpdateDiceCount()
    {
        OnDiceCountChanged?.Invoke();
    }

    public int GetDiceCount()
    {
        int count = 0;
        for (int i = 0; i < _dice.Count; i++)
        {
            if (_dice[i].IsDead()) continue;
            count++;
        }
        return count;
    }

    public void CollectDice()
    {
        // Instanciate a van scene
        var van = (PackedScene) GD.Load("res://Prefabs/Van.tscn");
        var instance = (Van) van.Instance();
        AddChild(instance);

        instance.SetDice(_dice);
    }

    public void DiceMoved()
    {
        bool assigned = false;

        for (int i = 0; i < _dice.Count(); i++)
        {
            if (_dice[i].IsInPlay)
            {
                assigned = true;
                break;
            }
        }

        if (assigned)
        {
            OnDiceAssigned?.Invoke();
        }
        else
        {
            OnDiceNotAssigned?.Invoke();
        }
    }
}
