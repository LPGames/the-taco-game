using System;

namespace TacoGame.Scripts.States
{
    public class PlayerConfirmDice : IState
    {
        public static event Action OnExit;
        public static event Action OnReset;

        private bool _confirmed = false;

        public void Enter()
        {
            GUI.OnNextButtonPressed += OnPlayerConfirmed;
            DiceController.OnDiceNotAssigned += OnDiceRemoved;
        }

        public void Exit()
        {
            GUI.OnNextButtonPressed -= OnPlayerConfirmed;
            DiceController.OnDiceNotAssigned -= OnDiceRemoved;

            if (_confirmed)
            {
                OnExit?.Invoke();
            }
            else
            {
                OnReset?.Invoke();
            }
        }

        private void OnDiceRemoved()
        {
            _confirmed = false;
            Exit();
        }

        private void OnPlayerConfirmed()
        {
            _confirmed = true;
            Exit();
        }
    }
}
