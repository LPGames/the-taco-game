using System;

namespace TacoGame.Scripts.States
{
    public class ShuffleCards : IState
    {
        public static event Action OnEnter;
        public static event Action OnExit;

        private CardManager _cardManager;
        private DiceController _diceController;

        public ShuffleCards(CardManager cardManager, DiceController diceController)
        {
            _cardManager = cardManager;
            _diceController = diceController;
        }

        public void Enter()
        {
            OnEnter?.Invoke();

            DoCollectDice();
        }

        private void DoCollectDice()
        {
            Van.OnDiceCollected += DoShuffleCards;

            _diceController.CollectDice();
        }

        private void DoShuffleCards()
        {
            Van.OnDiceCollected -= DoShuffleCards;
            CardManager.OnCardsShuffled += Exit;

            _cardManager.ShuffleCards();
            _diceController.MoveDiceToStart();
        }

        public void Exit()
        {
            CardManager.OnCardsShuffled -= Exit;
            OnExit?.Invoke();
        }
    }
}
