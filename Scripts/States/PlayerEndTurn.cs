using System;
using Godot;

namespace TacoGame.Scripts.States
{
    public class PlayerEndTurn : IState
    {
        public static event Action OnEnter;
        public static event Action OnExit;

        private DiceController _diceController;

        public PlayerEndTurn(DiceController diceController)
        {
            _diceController = diceController;
        }

        public void Enter()
        {
            OnEnter?.Invoke();
            GUI.OnNextButtonPressed += OnPlayerConfirmed;

        }

        public void Exit()
        {
            GUI.OnNextButtonPressed -= OnPlayerConfirmed;
            OnExit?.Invoke();
        }

        private void OnPlayerConfirmed()
        {
            Exit();
        }
    }
}
