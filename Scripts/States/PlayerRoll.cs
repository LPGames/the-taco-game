using System;

namespace TacoGame.Scripts.States
{
    public class PlayerRoll : IState
    {
        public static event Action OnExit;

        public void Enter()
        {
            GUI.OnRollButtonPressed += OnPlayerRolled;
        }

        public void Exit()
        {
            GUI.OnRollButtonPressed -= OnPlayerRolled;
            OnExit?.Invoke();
        }

        private void OnPlayerRolled()
        {
            Exit();
        }
    }
}
