namespace TacoGame.Scripts.States
{
    public interface IState
    {
        void Enter();

        void Exit();
    }
}
