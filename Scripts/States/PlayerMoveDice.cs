using System;

namespace TacoGame.Scripts.States
{
    public class PlayerMoveDice : IState
    {
        public static event Action OnExit;

        private CardManager _cardManager;

        public PlayerMoveDice(CardManager cardManager)
        {
            _cardManager = cardManager;
        }

        public void Enter()
        {
            _cardManager.TurnCardsDown();
            DiceController.OnDiceAssigned += OnDiceAssigned;
        }

        public void Exit()
        {
            DiceController.OnDiceAssigned -= OnDiceAssigned;
            OnExit?.Invoke();
        }

        private void OnDiceAssigned()
        {
            Exit();
        }
    }
}
