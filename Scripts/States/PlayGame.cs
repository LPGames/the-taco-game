using System;
using Godot;

namespace TacoGame.Scripts.States
{
    public class PlayGame : IState
    {
        public static event Action OnEnter;
        public static event Action OnExit;

        public void Enter()
        {
            DiceController.OnAllDiceDelivered += Exit;
            OnEnter?.Invoke();
        }

        public void Exit()
        {
            DiceController.OnAllDiceDelivered -= Exit;
            OnExit?.Invoke();
        }
    }
}
