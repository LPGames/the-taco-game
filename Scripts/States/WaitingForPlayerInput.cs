using System;

namespace TacoGame.Scripts.States
{
    public class WaitingForPlayerInput : IState
    {
        public static event Action OnExit;

        public void Enter()
        {
            CardHolder.OnCardHolderSelected += OnInputReceived;
        }

        public void Exit()
        {
            CardHolder.OnCardHolderSelected -= OnInputReceived;
            OnExit?.Invoke();
        }

        private void OnInputReceived()
        {
            Exit();
        }
    }
}
