using System;

namespace TacoGame.Scripts.States
{
    public class GameOver : IState
    {
        public static event Action OnEnter;

        private GUI _gui;

        public GameOver(GUI gui)
        {
            _gui = gui;
        }

        public void Enter()
        {
            OnEnter?.Invoke();
            _gui.ShowPopup();
        }

        public void Exit()
        {
            throw new NotImplementedException();
        }
    }
}
