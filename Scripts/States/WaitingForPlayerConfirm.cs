using System;

namespace TacoGame.Scripts.States
{
    public class WaitingForPlayerConfirm : IState
    {
        public static event Action OnExit;

        public void Enter()
        {
            GUI.OnNextButtonPressed += OnPlayerConfirmed;
        }

        public void Exit()
        {
            GUI.OnNextButtonPressed -= OnPlayerConfirmed;
            OnExit?.Invoke();
        }

        private void OnPlayerConfirmed()
        {
            Exit();
        }
    }
}
