using Godot;
using System;

public enum CardType
{
    None = 0,
    Buff = 1,
    Debuff = 2,
    Exhaust = 3,
    ClearPath = 4,
    Destroy = 5,
}

public class CardVariant : Node
{
    [Export] public Texture Texture;
    [Export] public CardType Type;
    [Export] public int Value;
    [Export] public int Qualifier;
}
