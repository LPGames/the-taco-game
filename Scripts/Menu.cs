using Godot;

public class Menu : Node
{
    private SFX _sfx;
    private MusicManager _musicManager;

    public override void _Ready()
    {
        _sfx = GetNode<SFX>("/root/SFX");
        _musicManager = GetNode<MusicManager>("/root/MusicManager");
    }

    public void OnButtonPressed()
    {
        _sfx.PlaySound("click.wav");

        GetParent().AddChild((Tutorial) GD.Load<PackedScene>("res://Tutorial.tscn").Instance());
        QueueFree();
    }

    public void _OnMusicToggled(bool buttonPressed)
    {
        _musicManager.Toggle(buttonPressed);
        _sfx.PlaySound("put.wav");
    }

    public void _OnSFXToggled(bool buttonPressed)
    {
        _sfx.Toggle(buttonPressed);
        _sfx.PlaySound("put.wav");
    }
}
