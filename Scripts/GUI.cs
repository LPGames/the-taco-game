using System;
using System.Threading.Tasks;
using Godot;
using TacoGame.Scripts.States;

public class GUI : Control
{
    public static event Action OnNextButtonPressed;
    public static event Action OnRollButtonPressed;

    private SFX _sfx;
    private StateMachine _stateMachine;
    private PointsManager _pointsManager;
    private DiceController _diceController;

    private AnimationPlayer _moneyAnimation, _diceAnimation, _turnAnimation;
    private TextureButton _backBtn, _nextBtn, _menuBtn, _rollBtn;
    private Label _instructions, _moneyAmount, _diceCounter, _turnCounter;
    private Popup _gameOverPopup;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _sfx = GetNode<SFX>("/root/SFX");
        _stateMachine = GetNode<StateMachine>("/root/StateMachine");
        _pointsManager = GetNode<PointsManager>("/root/PointsManager");
        _diceController = GetNode<DiceController>("/root/Game/Dice");

        _backBtn = GetNode<TextureButton>("MarginContainer/Control/BackBtn");
        _nextBtn = GetNode<TextureButton>("MarginContainer/Control/NextBtn");
        _menuBtn = GetNode<TextureButton>("MarginContainer/Control/MenuBtn");
        _rollBtn = GetNode<TextureButton>("MarginContainer/Control/RollBtn");
        _instructions = GetNode<Label>("MarginContainer/Control/Instructions");
        _moneyAmount = GetNode<Label>("MarginContainer/Control/Money/Amount");
        _diceCounter = GetNode<Label>("MarginContainer/Control/Dice/Amount");
        _turnCounter = GetNode<Label>("MarginContainer/Control/Turns/Amount");
        _gameOverPopup = GetNode<Popup>("MarginContainer/Control/PopupWrapper/GameOverPopup");

        _moneyAnimation = GetNode<AnimationPlayer>("MarginContainer/Control/Money/AnimationPlayer");
        _diceAnimation = GetNode<AnimationPlayer>("MarginContainer/Control/Dice/AnimationPlayer");
        _turnAnimation = GetNode<AnimationPlayer>("MarginContainer/Control/Turns/AnimationPlayer");

        CallDeferred(nameof(InitializeGUI));

    }

    private async Task InitializeGUI()
    {
        UpdateMoneyAmount();
        UpdateDiceCounter();
    }

    #region ButtonAccess

    public void ShowPopup()
    {
        _gameOverPopup.Popup_();
    }

    public void ChangeButton(string buttonString, string action)
    {
        TextureButton button;
        switch (buttonString)
        {
            case "back":
                button = _backBtn;
                break;
            case "next":
                button = _nextBtn;
                break;
            case "menu":
                button = _menuBtn;
                break;
            case "roll":
                button = _rollBtn;
                break;
            default:
                GD.PrintErr("No button found for: " + buttonString);
                return;
        }

        switch (action)
        {
            case "hide":
                button.Hide();
                break;
            case "show":
                button.Show();
                break;
            case "disable":
                button.Disabled = true;
                break;
            case "enable":
                button.Disabled = false;
                break;
        }
    }

    #endregion

    #region ButtonEvents

    public void _OnBackButtonPressed()
    {
        GD.Print("1");
        _sfx.PlaySound("click.wav");
    }

    public void _OnNextButtonPressed()
    {
        OnNextButtonPressed?.Invoke();
        GD.Print("2");
        _sfx.PlaySound("click.wav");
    }

    public void _OnRollButtonPressed()
    {
        OnRollButtonPressed?.Invoke();
        GD.Print("3");
        _sfx.PlaySound("click.wav");

    }

    public void _OnMenuButtonPressed()
    {
        GD.Print("4");
        _sfx.PlaySound("click.wav");
    }

    #endregion

    public void UpdateInstructions(string name)
    {
        string Instructions = "";

        switch (name)
        {
            case nameof(Start):
                Instructions = "Lets go!";
                break;
            case nameof(PlayerRoll):
                Instructions = "Check your cards, then roll the dice to start";
                break;
            case nameof(PlayerMoveDice):
                Instructions = "Assign dice to a restaurant of your choise";
                break;
            case nameof(PlayerConfirmDice):
                Instructions = "Ready to go?";
                break;
            case nameof(PlayGame):
                Instructions = "Delivering the food";
                break;
            case nameof(ShuffleCards):
                Instructions = "Shuffling...";
                break;
            case nameof(WaitingForPlayerInput):
                Instructions = "Pick your dice";
                break;
            case nameof(WaitingForPlayerConfirm):
                Instructions = "Press Next to when you're ready";
                break;
        }

        _instructions.Text = Instructions;
    }

    private void UpdateMoneyAmount()
    {
        _moneyAmount.Text = _pointsManager.GetPoints().ToString();
        _moneyAnimation.Play("MoneyBuff");
    }

    private void UpdateDiceCounter()
    {
        _diceCounter.Text = "x" + _diceController.GetDiceCount().ToString();
        _diceAnimation.Play("MoneyBuff");
    }

    public void UpdateTurn(int turn)
    {
        _turnCounter.Text = turn.ToString() + "/5";
        _turnAnimation.Play("MoneyBuff");
    }

    public override void _EnterTree()
    {
        CardManager.OnNoMoneyToMove += UpdateMoneyAmount;
        PointsManager.OnPointsChanged += UpdateMoneyAmount;
        DiceController.OnDiceCountChanged += UpdateDiceCounter;
    }

    public override void _ExitTree()
    {
        CardManager.OnNoMoneyToMove -= UpdateMoneyAmount;
        PointsManager.OnPointsChanged -= UpdateMoneyAmount;
        DiceController.OnDiceCountChanged -= UpdateDiceCounter;
    }
}
