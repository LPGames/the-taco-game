using Godot;
using System;
using System.Collections.Generic;

public class Tutorial : Node
{
    public static event Action OnTutorialDismissed;

    private SFX _sfx;

    private List<TutorialStep> _tutorialSteps = new List<TutorialStep>();
    private Sprite _image;
    private Label _description;
    private TextureButton _nextButton, _skipButton, _startButton;

    private int _currentStep = 0;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        _sfx = GetNode<SFX>("/root/SFX");

        _image = GetNode<Sprite>("Image");
        _description = GetNode<Label>("Description");
        _nextButton = GetNode<TextureButton>("NextButton");
        _skipButton = GetNode<TextureButton>("SkipButton");
        _startButton = GetNode<TextureButton>("StartButton");

        LoadSteps();

        ShowStepNextStep();
    }

    private void ShowStepNextStep()
    {
        if (_currentStep < _tutorialSteps.Count - 1)
        {
            _nextButton.Visible = true;
            _skipButton.Visible = true;
            _startButton.Visible = false;
        }

        if (_currentStep == _tutorialSteps.Count - 1)
        {
            _nextButton.Visible = false;
            _skipButton.Visible = false;
            _startButton.Visible = true;
        }

        var Step = _tutorialSteps[_currentStep];

        _image.Texture = Step.Image;
        _description.Text = Step.Text;
        _nextButton.Visible = _currentStep < _tutorialSteps.Count - 1;

        if (Step.Animated)
        {
            var animatedTexture = (AnimatedTexture) Step.Image;
            animatedTexture.CurrentFrame = 0;
            _image.Texture = animatedTexture;
        }

        _currentStep++;
    }

    private void LoadSteps()
    {
        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "Your mission is to deliver as much food as possible to the hungry people of the castle.",
            Image = GD.Load<Texture>("res://icon.png")
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "Your riders only have a limited amount of stamina so be cautious.",
            Image = GD.Load<Texture>("res://Assets/Dice/4.png")
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "Potholes slow you down reducing your stamina.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/debuff/debuff.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "If you have enough energy you will get past the barrier before it closes.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/exhaust_fail/exhaust_fail.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "But if you dont your rider will fail and need to return until the next round.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/exhaust_success/exhaust_success.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text =
                "If you are too tired you will not make the jump over the river and there will not be another ride for you.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/destroy_success/destroy_success.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "But a good downhill slope may give you an energy boost to keep going.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/buff/buff.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "And if you can hold on to a speedy rocket it will guide you safely to the end.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/clear_success/clear_success.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "Once you have some money you can spend it to swap obstacles.",
            Image = GD.Load<AnimatedTexture>("res://Assets/Tutorial/swaps/swap.tres"),
            Animated = true
        });

        _tutorialSteps.Add(new TutorialStep()
        {
            Text = "Now go and make us all proud (/rich)!",
            Image = GD.Load<Texture>("res://icon.png")
        });
    }

    public void _OnNextButtonPressed()
    {
        _sfx.PlaySound("click.wav");
        ShowStepNextStep();
    }

    public void _OnSkipButtonPressed()
    {
        _OnStartButtonPressed();
    }

    public void _OnStartButtonPressed()
    {
        _sfx.PlaySound("click.wav");
        OnTutorialDismissed?.Invoke();

        QueueFree();
        return;
    }

    struct TutorialStep
    {
        public string Text;
        public Texture Image;
        public bool Animated;
    }
}
