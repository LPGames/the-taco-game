using System.Threading.Tasks;
using Godot;

public class Card : Node2D
{
    [Signal]
    delegate void OnFocusCompleted();

    private CardManager _cardManager;
    private SFX _sfx;
    private PointsManager _pointsManager;

    private Area2D _area2D;

    private CardVariant _cardVariant;

    private bool _selected = false;
    private Vector2 _restPoint;
    private CardHolder _currentHolder;

    private Sprite _sprite;

    private float _clickTime = 0;
    private bool _dragging => _clickTime > 0.2f;

    private bool _focus = false;

    private Texture _defaultTexture, _backTexture;

    public override void _Ready()
    {
        _cardManager = GetNode<CardManager>("/root/CardManager");
        _pointsManager = GetNode<PointsManager>("/root/PointsManager");
        _sfx = GetNode<SFX>("/root/SFX");
        _restPoint = GlobalPosition;
        _sprite = GetChild<Sprite>(0);
        _area2D = _sprite.GetChild<Area2D>(0);

        SetCardVariant();

        // Set sprite texture from image file.
        _defaultTexture = _sprite.Texture;
        _backTexture = GD.Load<Texture>("res://Assets/Cards/question.png");
        _sprite.Texture = _backTexture;
    }

    private void SetCardVariant()
    {
        _cardVariant = _cardManager.GetRandomCardVariant();
        _sprite.Texture = _cardVariant.Texture;
    }

    public override void _Process(float delta)
    {
        if (_selected)
        {
            _clickTime += delta;
        }
        else
        {
            _clickTime = 0;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        if (_dragging)
        {
            // Lerp to mouse position
            GlobalPosition = GlobalPosition.LinearInterpolate(GetGlobalMousePosition(), 10 * delta);
        }
        else
        {
            GlobalPosition = GlobalPosition.LinearInterpolate(_restPoint, 10 * delta);
        }
    }

    private void _OnArea2DInputEvent(Node2D viewport, InputEvent inputEvent, int _)
    {
        if (_cardManager.CanMoveCards() == false) return;

        if (!(inputEvent is InputEventMouseButton))
        {
            return;
        }

        InputEventMouseButton mouseEvent = (InputEventMouseButton) inputEvent;

        if (!mouseEvent.IsPressed())
        {
            return;
        }

        if (mouseEvent.ButtonIndex != (int) ButtonList.Left)
        {
            return;
        }

        _selected = true;
    }

    public void _OnArea2DMouseEntered()
    {
        if (_cardManager.CanMoveCards() == false) return;

        ScaleUpCard();
    }
    public void _OnArea2DMouseExited()
    {
        if (_cardManager.CanMoveCards() == false) return;

        ScaleDownCard();
    }

    public override void _Input(InputEvent @event)
    {
        if (_cardManager.CanMoveCards() == false) return;

        if (!_selected && !_focus) return;

        if (!(@event is InputEventMouseButton))
        {
            return;
        }

        InputEventMouseButton mouseEvent = (InputEventMouseButton) @event;

        if (mouseEvent.IsPressed())
        {
            return;
        }

        if (mouseEvent.ButtonIndex != (int) ButtonList.Left)
        {
            return;
        }


        if (_dragging)
        {
            HandleDragRelease();
        }

        _selected = false;
    }

    private async void HandleFocus(bool playSound = true)
    {
        // tween scale increase.
        Tween tween_out = GetNode<Tween>("Tween");
        tween_out.InterpolateProperty(this, "scale", Scale, new Vector2(0, 1), 0.2f, Tween.TransitionType.Cubic,
            Tween.EaseType.Out);
        tween_out.Start();
        await ToSignal(tween_out, "tween_completed");

        if (_focus)
        {
            if (playSound)
            {
                _sfx.PlaySound("draw.wav");
            }

            _sprite.Texture = _defaultTexture;
        }
        else
        {
            if (playSound)
            {
                _sfx.PlaySound("select.wav");
            }

            _sprite.Texture = _backTexture;
        }

        Tween tween_in = GetNode<Tween>("Tween");
        tween_in.InterpolateProperty(this, "scale", Scale, new Vector2(1, 1), 0.2f, Tween.TransitionType.Cubic,
            Tween.EaseType.Out);
        tween_in.Start();

        await ToSignal(tween_in, "tween_completed");
        EmitSignal(nameof(OnFocusCompleted));
    }

    private void HandleDragRelease()
    {
        CardHolder chosenHolder = _cardManager.GetClosestHolder(GlobalPosition);

        if (chosenHolder == null)
        {
            return;
        }

        if (_pointsManager.GetPoints() <= 0)
        {
            _cardManager.NoMoneyToMoveCards();
            return;
        }

        // If there is already a card in the holder, try to swap them.
        if (chosenHolder.IsSelected)
        {
            // If the current card is not in a holder we cant swap.
            if (_currentHolder == null)
            {
                return;
            }

            chosenHolder.Card.MoveToHolder(_currentHolder);
        }

        _pointsManager.AddPoints(-1);
        MoveToHolder(chosenHolder);
    }

    private async void ScaleUpCard()
    {
        Tween tween = GetNode<Tween>("Tween");
        tween.InterpolateProperty(this, "scale", Scale, new Vector2(1, 1) * 1.5f, 0.2f, Tween.TransitionType.Cubic,
            Tween.EaseType.Out);
        tween.Start();
        await ToSignal(tween, "tween_completed");
    }

    private async void ScaleDownCard()
    {
        Tween tween = GetNode<Tween>("Tween");
        tween.InterpolateProperty(this, "scale", Scale, new Vector2(1, 1), 0.2f, Tween.TransitionType.Cubic,
            Tween.EaseType.Out);
        tween.Start();
        await ToSignal(tween, "tween_completed");
    }

    public void MoveToHolder(CardHolder holder)
    {
        holder.Select(this);
        _restPoint = holder.GlobalPosition;
        _currentHolder = holder;
    }

    public async Task Interaction(Dice dice)
    {
        _focus = true;
        HandleFocus();

        await ToSignal(this, nameof(OnFocusCompleted));

        await HandleInteraction(dice);
    }

    private async Task HandleInteraction(Dice dice)
    {
        await ToSignal(GetTree().CreateTimer(0.2f), "timeout");

        int score = dice.GetDiceScore();
        if (score <= _cardVariant.Qualifier)
        {
            return;
        }

        switch (_cardVariant.Type)
        {
            case CardType.Buff:
                _sfx.PlaySound("select_twinkle.wav");
                score += _cardVariant.Value;
                break;
            case CardType.Debuff:
                _sfx.PlaySound("damage.wav");
                score -= _cardVariant.Value;
                break;
            case CardType.Destroy:
                if (score < _cardVariant.Value)
                {
                    dice.Death();
                    return;
                }

                break;
            case CardType.Exhaust:
                if (score < _cardVariant.Value)
                {
                    score = 0;
                    dice.OutOfStamina();
                }

                break;
            case CardType.ClearPath:
                _sfx.PlaySound("select_twinkle.wav");
                dice.ClearPath();
                break;
        }

        await ToSignal(GetTree().CreateTimer(0.25f), "timeout");

        dice.SetDiceScore(score);
    }

    public void Activate()
    {
        _area2D.SetDeferred("monitorable", true);
    }

    public void Deactivate()
    {
        _area2D.SetDeferred("monitorable", false);
    }

    public void SetFocus(bool focus)
    {
        if (_focus == focus)
        {
            return;
        }

        _focus = focus;
        HandleFocus(false);
    }
}
