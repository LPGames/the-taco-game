using Godot;

public enum SpriteIcon
{
    Taco = 0,
    Sandwich = 1,
    Rice = 2,
    IceCream = 3,
}

public class Restaurant : Node2D
{
    [Export]
    public int Reward;

    [Export]
    public SpriteIcon Icon;

    private Label _money;

    public bool IsSelected { get; private set; }

    public Dice Dice { get; private set; }

    public override void _Ready()
    {
        Sprite icon = GetNode<Sprite>("Icon");
        icon.Texture = GD.Load<Texture>("res://Assets/Icons/" + Icon + ".png");
        _money = GetNode<Label>("Image/Money");
        _money.Text = "$" + Reward.ToString();
    }

    public void Select(Dice dice)
    {
        var nodes = GetTree().GetNodesInGroup("restaurant");
        foreach (var node in nodes)
        {
            if (!(node is Restaurant)) continue;

            Restaurant holder = node as Restaurant;

            if (!holder.IsSelected) continue;

            if (holder.Dice == dice)
            {
                holder.Deselect();
            }
        }

        IsSelected = true;
        Dice = dice;
    }

    public void Deselect()
    {
        IsSelected = false;
        Dice = null;
    }

}
