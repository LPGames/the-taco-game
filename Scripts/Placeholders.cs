using Godot;
using System;
using TacoGame.Scripts.States;

public class Placeholders : Node2D
{
    private CardHolder[] _cardHolders;

    private CardManager _cardManager;

    public override void _Ready()
    {
        _cardManager = GetNode<CardManager>("/root/CardManager");

        var nodes = GetTree().GetNodesInGroup("zone");
        _cardHolders = new CardHolder[nodes.Count];
        for (int i = 0; i < nodes.Count; i++)
        {
            _cardHolders[i] = nodes[i] as CardHolder;
        }

        _cardManager.SetCardHolders(_cardHolders);
    }

    private void ActivateCards()
    {
        foreach (CardHolder cardHolder in _cardHolders)
        {
            if (!cardHolder.IsSelected) continue;

            cardHolder.Card.Activate();
        }
    }
    private void DeactivateCards()
    {
        foreach (CardHolder cardHolder in _cardHolders)
        {
            if (!cardHolder.IsSelected) continue;

            cardHolder.Card.Deactivate();
        }
    }


    public override void _EnterTree()
    {
        PlayGame.OnEnter += ActivateCards;
        PlayGame.OnExit += DeactivateCards;
    }

    public override void _ExitTree()
    {
        PlayGame.OnEnter -= ActivateCards;
        PlayGame.OnExit -= DeactivateCards;
    }
}
