using Godot;
using System;
using System.Collections.Generic;

public class Van : Node2D
{
    public static event Action OnDiceCollected;

    private List<Dice> _dice = new List<Dice>();
    private Dice _currentDice;
    private Vector2 _target;
    private bool _hasTarget = false;

    private bool _hasCollectedDice = false;

    [Export] public Vector2 StartPosition, EndPosition;

    public void SetDice(List<Dice> dices)
    {
        foreach (var dice in dices)
        {
            _dice.Add(dice);
        }
    }

    public override void _Ready()
    {
        GlobalPosition = StartPosition;
    }

    public override void _PhysicsProcess(float delta)
    {
        if (!_hasTarget)
        {
            GetNextTarget();

            // If there were no dice to collect just finish the collection.
            if (_target == EndPosition && !_hasCollectedDice)
            {
                FinishCollection();
            }
        }

        if (_hasTarget)
        {
            GlobalPosition = GlobalPosition.LinearInterpolate(_target, 3 * delta);
        }

        if (GlobalPosition.DistanceTo(_target) < 100)
        {
            if (_currentDice != null)
            {
                _hasCollectedDice = true;
                _currentDice.Visible = false;
                _currentDice = null;
            }

            _hasTarget = false;
            if (_target == EndPosition)
            {
                FinishCollection();
            }
        }
    }

    private void FinishCollection()
    {
        OnDiceCollected?.Invoke();
        QueueFree();
    }

    private void GetNextTarget()
    {
        float distance = 0;
        foreach (var dice in _dice)
        {
            if (!dice.CanBeCollected)
            {
                continue;
            }

            var diceDistance = dice.GlobalPosition.DistanceTo(GlobalPosition);
            if (distance == 0 || diceDistance < distance)
            {
                distance = diceDistance;
                _target = dice.GlobalPosition;
                _currentDice = dice;
                _hasTarget = true;
            }
        }

        if (!_hasTarget)
        {
            _target = EndPosition;
            _hasTarget = true;
        }
    }
}
