using Godot;
using System;

public class Highscores : VBoxContainer
{
    public static event Action OnHighscoresRequested;

    // Called when the node enters the scene tree for the first time.
    private void GetHighscores()
    {
        OnHighscoresRequested?.Invoke();
    }

    private void UpdateHighScores(string[] highscores)
    {
        foreach (Node child in GetChildren())
        {
            child.QueueFree();
        }

        foreach (var score in highscores)
        {
            // Instanciate a highscore scene.
            Score highscore = (Score) GD.Load<PackedScene>("res://Prefabs/Score.tscn").Instance();
            highscore.UpdateScore(score);
            AddChild(highscore);
        }
    }


    public override void _EnterTree()
    {
        GameOverPopup.OnPopupShowing += GetHighscores;
        CSBridge.OnHighscoresFetched += UpdateHighScores;
    }

    public override void _ExitTree()
    {
        GameOverPopup.OnPopupShowing -= GetHighscores;
        CSBridge.OnHighscoresFetched -= UpdateHighScores;
    }
}
