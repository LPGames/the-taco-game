using Godot;

public class Score : Control
{
    private Label _score;
    private string _scoreToShow;

    public override void _Ready()
    {
        _score = GetNode<Label>("ScoreLabel");
        _score.Text = _scoreToShow;
    }

    public void UpdateScore(string score)
    {
        _scoreToShow = score;
    }
}
