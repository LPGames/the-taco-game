using Godot;
using System;

public class CardHolder : Node2D
{
    public static event Action OnCardHolderSelected;

    public bool IsSelected { get; private set; }
    public Card Card { get; private set; }

    public void Select(Card card)
    {
        var nodes = GetTree().GetNodesInGroup("zone");
        foreach (var node in nodes)
        {
            if (!(node is CardHolder)) continue;

            CardHolder holder = node as CardHolder;

            if (!holder.IsSelected) continue;

            if (holder.Card == card)
            {
                holder.Deselect();
            }
        }

        Sprite image = GetChild<Sprite>(0);
        image.Modulate = new Color(0, 0, 0, 0.5f);

        IsSelected = true;
        Card = card;

        OnCardHolderSelected?.Invoke();
    }

    public void Deselect()
    {
        Sprite image = GetChild<Sprite>(0);
        image.Modulate = new Color(1, 1, 1, 0.5f);

        IsSelected = false;
        Card = null;
    }
}
