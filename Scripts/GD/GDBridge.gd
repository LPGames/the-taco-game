extends Node

signal points_fetched(points)

var score_name = "highscore"

func _ready():
	print("initializing gotm")
	Gotm.initialize()

func _on_CSBridge_PointsScored(points):
	yield(GotmScore.create(score_name, points), "completed")
	print("points scored")
	print(points)

func _on_CSBridge_PointsFetch():
	var top_leaderboard = GotmLeaderboard.new()

	top_leaderboard.name = score_name
	var top_scores = yield(top_leaderboard.get_scores(), "completed")

	var scores = []

	print("Top scores:")

	for score in top_scores:
		var name = "anonymous"
		# Fetch the Gotm-registered user that created score1.
		var user: GotmUser = yield(GotmUser.fetch(score.user_id), "completed")
		if user:
			name = user.display_name

		scores.push_back(name + ":" + str(score.value))
	emit_signal("points_fetched", scores)
