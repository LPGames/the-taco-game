using Godot;
using System;
using System.Threading.Tasks;
using TacoGame.Scripts.States;

public class StateMachine : Node
{
    public static event Action OnTurnFinished;

    private IState _currentState = null;

    private GUI _gui;
    private CardManager _cardManager;
    private DiceController _diceController;

    public override async void _Ready()
    {
        _gui = GetNode<GUI>("/root/Game/GUI");
        _cardManager = GetNode<CardManager>("/root/CardManager");
        _diceController = GetNode<DiceController>("/root/Game/Dice");

        await InitializeButtons();

        _currentState = new Start();
        _gui.ChangeButton("back", "hide");
    }

    public void StartNewTurn()
    {
        IState startingState = new ShuffleCards(_cardManager, _diceController);
        TransitionTo(startingState);
    }

    public void GameOver()
    {
        IState nextState = new GameOver(_gui);
        TransitionTo(nextState);
    }

    private async Task InitializeButtons()
    {
        await ToSignal(_gui, "ready");
        _gui.ChangeButton("next", "hide");
    }

    private void TransitionTo(IState state)
    {
        if (_currentState != null)
        {
            GD.Print("Transitioning from: " + _currentState.GetType().Name + " to: " + state.GetType().Name);
        }


        _gui.UpdateInstructions(state.GetType().Name);
        _currentState = state;
        _currentState.Enter();
    }

    private void OnExitPlayerRoll()
    {
        _gui.ChangeButton("roll", "hide");

        IState nextState = new PlayerMoveDice(_cardManager);
        TransitionTo(nextState);
    }

    private void OnExitPlayerMoveDice()
    {
        _gui.ChangeButton("next", "show");

        IState nextState = new PlayerConfirmDice();
        TransitionTo(nextState);
    }

    private void OnExitPlayerConfirmDice()
    {
        _gui.ChangeButton("next", "hide");

        IState nextState = new PlayGame();
        TransitionTo(nextState);
    }

    private void OnResetPlayerConfirmDice()
    {
        _gui.ChangeButton("next", "hide");

        IState nextState = new PlayerMoveDice(_cardManager);
        TransitionTo(nextState);
    }

    private void OnExitPlayGame()
    {
        _gui.ChangeButton("next", "show");

        IState nextState = new PlayerEndTurn(_diceController);
        TransitionTo(nextState);
    }

    private void OnExitPlayerEndTurn()
    {
        _gui.ChangeButton("next", "hide");
        OnTurnFinished?.Invoke();
    }

    private void OnExitShuffleCards()
    {
        _gui.ChangeButton("roll", "show");

        IState nextState = new PlayerRoll();
        TransitionTo(nextState);
    }

    public override void _EnterTree()
    {
        PlayerRoll.OnExit += OnExitPlayerRoll;
        PlayerMoveDice.OnExit += OnExitPlayerMoveDice;
        PlayerConfirmDice.OnExit += OnExitPlayerConfirmDice;
        PlayerConfirmDice.OnReset += OnResetPlayerConfirmDice;
        PlayGame.OnExit += OnExitPlayGame;
        PlayerEndTurn.OnExit += OnExitPlayerEndTurn;
        ShuffleCards.OnExit += OnExitShuffleCards;

        DiceController.OnGameOver += GameOver;
    }

    public override void _ExitTree()
    {
        PlayerRoll.OnExit -= OnExitPlayerRoll;
        PlayerMoveDice.OnExit -= OnExitPlayerMoveDice;
        PlayerConfirmDice.OnExit -= OnExitPlayerConfirmDice;
        PlayerConfirmDice.OnReset -= OnResetPlayerConfirmDice;
        PlayGame.OnExit -= OnExitPlayGame;
        PlayerEndTurn.OnExit -= OnExitPlayerEndTurn;
        ShuffleCards.OnExit -= OnExitShuffleCards;

        DiceController.OnGameOver -= GameOver;
    }

    public string GetCurrentState()
    {
        return _currentState.GetType().Name;
    }
}
