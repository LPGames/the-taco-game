using System.Collections.Generic;
using Godot;

public class SFX : Node
{
    private AudioStreamPlayer _player;

    private Dictionary<string, AudioStream> _sounds = new Dictionary<string, AudioStream>();

    private bool _isMuted = false;

    public override void _Ready()
    {
        GD.Randomize();

        // Create new audio stream player.
        var sound = new AudioStreamPlayer();
        // Add it to the scene.
        AddChild(sound);

        _player = sound;
        _player.VolumeDb = -20;

        LoadSounds();
    }

    private void LoadSounds()
    {
        string[] files =
        {
            "click.wav",
            "damage.wav",
            "dice1.ogg",
            "dice2.ogg",
            "dice3.ogg",
            "discard_twinkle.wav",
            "draw.wav",
            "glassbell.wav",
            "mulligan.wav",
            "put.wav",
            "put_twinkle.wav",
            "select.wav",
            "select_twinkle.wav"
        };

        foreach (var fileName in files)
        {
            var audioStream = ResourceLoader.Load("res://Assets/SFX/" + fileName) as AudioStream;
            _sounds.Add(fileName, audioStream);
        }
    }

    public async void PlaySound(string sound)
    {
        if (_isMuted) return;

        if (!_sounds.ContainsKey(sound)) return;

        var player = (AudioStreamPlayer) _player.Duplicate();
        AddChild(player);

        var clip = _sounds[sound];

        // Set the sound file.
        player.Stream = clip;
        // Play the sound.
        player.Play();

        player.PitchScale = 1.0f + (float) GD.RandRange(-0.1f, 0.1f);

        await ToSignal(player, "finished");
        player.QueueFree();
    }


    public void PlayDiceSound()
    {
        int value = (int) (GD.Randi() % 3 + 1);
        PlaySound("dice" + value + ".ogg");
    }

    public void Toggle(bool on)
    {
        if (on)
        {
            _isMuted = false;
            // _player.VolumeDb = -20;
        }
        else
        {
            _isMuted = true;
            // _player.VolumeDb = -80;
        }
    }
}
