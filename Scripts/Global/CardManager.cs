using Godot;
using System;
using TacoGame.Scripts.States;

public class CardManager : Node
{
    public static event Action OnCardsShuffled;
    public static event Action OnNoMoneyToMove;

    private StateMachine _stateMachine;

    private CardHolder[] _cardHolders;
    private CardVariant[] _cardVariants;
    public bool CanMoveCards()
    {
        if (_stateMachine.GetCurrentState() == nameof(PlayerRoll))
        {
            return true;
        }

        return false;
    }


    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GD.Randomize();
        _stateMachine = GetNode<StateMachine>("/root/StateMachine");

        LoadVariants();
    }

    private void LoadVariants()
    {
        var variant = (PackedScene) GD.Load("res://Prefabs/CardVariants/Buff.tscn");
        var buff = (CardVariant) variant.Instance();

        variant = (PackedScene) GD.Load("res://Prefabs/CardVariants/Debuff.tscn");
        var debuff = (CardVariant) variant.Instance();

        variant = (PackedScene) GD.Load("res://Prefabs/CardVariants/Exhaust.tscn");
        var exhaust = (CardVariant) variant.Instance();

        variant = (PackedScene) GD.Load("res://Prefabs/CardVariants/Destroy.tscn");
        var destroy = (CardVariant) variant.Instance();

        variant = (PackedScene) GD.Load("res://Prefabs/CardVariants/Clear.tscn");
        var clear = (CardVariant) variant.Instance();

        _cardVariants = new []
        {
            buff, debuff, exhaust, destroy, clear
            // destroy
        };
    }


    public void SetCardHolders(CardHolder[] cardHolders)
    {
        _cardHolders = cardHolders;
    }

    public CardHolder GetClosestHolder(Vector2 cardPosition)
    {
        int shortestDistance = 150;
        CardHolder chosenHolder = null;
        foreach (var cardHolder in _cardHolders)
        {
            int distance = (int) cardPosition.DistanceTo(cardHolder.GlobalPosition);

            if (distance < shortestDistance)
            {
                chosenHolder = cardHolder;
                shortestDistance = distance;
            }
        }

        return chosenHolder;
    }

    private async void AddToMap(Card card)
    {
        CardHolder _nextFreeHolder = null;
        foreach (CardHolder holder in _cardHolders)
        {
            if (!holder.Visible) continue;
            if (holder.IsSelected) continue;

            _nextFreeHolder = holder;
            break;
        }

        if (_nextFreeHolder == null) return;
        card.MoveToHolder(_nextFreeHolder);

        await ToSignal(GetTree().CreateTimer(0.5f), "timeout");
    }

    private void ClearMap() {
        foreach (CardHolder holder in _cardHolders)
        {
            holder.Deselect();
        }
    }

    public CardVariant GetRandomCardVariant()
    {
        int value = (int) (GD.Randi() % _cardVariants.Length);
        return _cardVariants[value];
    }

    public void ShuffleCards()
    {
        ClearMap();
        var cardsNode = GetNode("/root/Game/Cards");

        // remove all children from cardsNode.
        for (int i = cardsNode.GetChildCount() - 1; i >= 0; i--)
        {
            cardsNode.GetChild(i).QueueFree();
        }

        for (int i = 0; i < _cardHolders.Length; i++)
        {
            if (!_cardHolders[i].Visible) continue;

            var card = (PackedScene) GD.Load("res://Prefabs/Card.tscn");
            var instance = (Card) card.Instance();

            cardsNode.AddChild(instance);
            AddToMap(instance);
        }

        TurnCardsUp();
        OnCardsShuffled?.Invoke();
    }

    public void TurnCardsUp()
    {
        foreach (CardHolder holder in _cardHolders)
        {
            if (holder.IsSelected)
            {
                holder.Card.SetFocus(true);
            }
        }
    }

    public void TurnCardsDown()
    {
        foreach (CardHolder holder in _cardHolders)
        {
            if (holder.IsSelected)
            {
                holder.Card.SetFocus(false);
            }
        }
    }

    public void NoMoneyToMoveCards()
    {
        OnNoMoneyToMove?.Invoke();
    }
}
