using System.Collections.Generic;
using Godot;

public class MusicManager : Node
{

    private AudioStreamPlayer _player;

    private Dictionary<string, AudioStream> _sounds = new Dictionary<string, AudioStream>();

    public override void _Ready()
    {
        // Create new audio stream player.
        var sound = new AudioStreamPlayer();
        // Add it to the scene.
        AddChild(sound);

        _player = sound;
        _player.VolumeDb = -20;

        LoadSounds();
    }

    private void LoadSounds()
    {
        GD.Print("Music starting");

        // check for files in the directory
        // var dir = new Directory();
        // dir.Open("res://Assets/Music");
        // dir.ListDirBegin(true, true);
        // while (true)
        // {
        //     var file = dir.GetNext();
        //     if (file == "")
        //     {
        //         break;
        //     }
        //
        //     GD.Print("Found file: " + file + " in directory: " + dir.GetCurrentDir());
        //
        //     // Ignore files with a .import extension.
        //     if (!file.Contains(".import"))
        //     {
        //         continue;
        //     }
        //
        //     string fileName = file.Substring(0, file.IndexOf(".import"));
        //
        //     var audioStream = ResourceLoader.Load("res://Assets/Music/" + fileName) as AudioStream;
        //     _sounds.Add(fileName, audioStream);
        //
        //     GD.Print("Added filename: " + fileName + " to dictionary");
        //
        // }

        string fileName1 = "background.ogg";
        var audioStream1 = ResourceLoader.Load("res://Assets/Music/" + fileName1) as AudioStream;
        _sounds.Add(fileName1, audioStream1);

        // var audioStream2 = ResourceLoader.Load("res://Assets/Music/background.ogg") as AudioStream;
        // _sounds.Add("background.ogg", audioStream2);
    }


    public async void PlayMusic(string sound)
    {
        if (!_sounds.ContainsKey(sound))
        {
            return;
        }

        _player.Stream = _sounds[sound];
        // Play the sound.
        _player.Play();
    }


    public void Toggle(bool on)
    {
        if (on)
        {
            _player.StreamPaused = false;
        }
        else
        {
            _player.StreamPaused = true;
        }
    }
}
