using Godot;
using System;
using TacoGame.Scripts.States;

public class PointsManager : Node
{
    public static event Action OnPointsChanged;
    public static event Action<int> OnSubmitHighscore;

    private int _points = 0;

    public int GetPoints()
    {
        return _points;
    }

    public void AddPoints(int points)
    {
        _points += points;
        OnPointsChanged?.Invoke();
    }

    private void OnRestartGame()
    {
        _points = 0;
        OnPointsChanged?.Invoke();
    }

    private void SubmitHighscore()
    {
        OnSubmitHighscore?.Invoke(_points);
    }

    public override void _EnterTree()
    {
        GameOver.OnEnter += SubmitHighscore;
        Game.OnRestartGame += OnRestartGame;
    }

    public override void _ExitTree()
    {
        GameOver.OnEnter -= SubmitHighscore;
        Game.OnRestartGame -= OnRestartGame;
    }


}
