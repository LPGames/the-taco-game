using System;
using Godot;

public class Dice : Node2D
{
    private DiceController _diceController;
    private SFX _sfx;

    private Sprite _sprite;
    private Sprite _bikeSprite;
    private Sprite _graveSprite;
    private CPUParticles2D _winParticles, _loseParticles, _deathParticles;

    private bool _selected = false;
    private Vector2 _restPoint;
    private Restaurant _restaurant = null;

    private float _clickTime = 0;
    private bool _dragging => _clickTime > 0.2f;

    private bool _placed = false;
    private int _score;

    /* Gameplay vars */
    private Vector2 _rollPosition;
    private int _moveSpeed => 100 + (int) (100 * (float) GetDiceScore() / 2);
    private bool _dead = false;
    private bool _moving = false;
    private bool _clearing = false;
    private bool _collected = false;
    private bool _delivered = false;

    public void SetController(DiceController controller)
    {
        _diceController = controller;
    }

    public override void _Ready()
    {
        _sprite = GetChild<Sprite>(0);
        _bikeSprite = GetNode<Sprite>("Bike");
        _graveSprite = GetNode<Sprite>("Grave");
        _winParticles = GetNode<CPUParticles2D>("WinParticles");
        _loseParticles = GetNode<CPUParticles2D>("LoseParticles");
        _deathParticles = GetNode<CPUParticles2D>("DeathParticles");
        _sfx = GetNode<SFX>("/root/SFX");
    }

    private void _OnArea2DInputEvent(Node2D viewport, InputEvent inputEvent, int _)
    {
        if (_diceController.CanMoveDice() == false) return;

        if (!(inputEvent is InputEventMouseButton))
        {
            return;
        }

        InputEventMouseButton mouseEvent = (InputEventMouseButton) inputEvent;

        if (!mouseEvent.IsPressed())
        {
            return;
        }

        if (mouseEvent.ButtonIndex != (int) ButtonList.Left)
        {
            return;
        }

        _selected = true;
    }

    public async void _OnArea2DAreaEntered(Area2D area)
    {
        if (!area.IsInGroup("card_collision")) return;

        if (_clearing) return;

        Card card = area.GetParent().GetParentOrNull<Card>();
        if (card == null) return;

        card.Deactivate();

        StopMoving();

        await card.Interaction(this);

        StartMoving();
    }

    public void _OnArea2DMouseEntered()
    {
        if (_diceController.CanMoveDice() == false) return;

        ScaleUpDice();
    }
    public void _OnArea2DMouseExited()
    {
        if (_diceController.CanMoveDice() == false) return;

        ScaleDownDice();
    }

    public override void _Input(InputEvent @event)
    {
        if (_diceController.CanMoveDice() == false) return;

        if (!_selected) return;

        if (!(@event is InputEventMouseButton))
        {
            return;
        }

        InputEventMouseButton mouseEvent = (InputEventMouseButton) @event;

        if (mouseEvent.IsPressed())
        {
            return;
        }

        if (mouseEvent.ButtonIndex != (int) ButtonList.Left)
        {
            return;
        }

        if (_dragging)
        {
            HandleDragRelease();
        }

        _selected = false;
    }

    public override void _Process(float delta)
    {
        if (_selected)
        {
            _clickTime += delta;
        }
        else
        {
            _clickTime = 0;
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        if (_dead) return;

        HandleDragging(delta);
        HandleMoving(delta);
        HandleClearing(delta);
    }

    private void HandleClearing(float delta)
    {
        if (!_clearing) return;

        GlobalPosition = GlobalPosition.LinearInterpolate(new Vector2(1500, GlobalPosition.y), 10 * delta);
    }

    private void HandleMoving(float delta)
    {
        if (!_moving) return;

        if (_clearing) return;


        GlobalPosition = GlobalPosition.LinearInterpolate(GlobalPosition + new Vector2(1, 0), _moveSpeed * delta);
    }

    private void HandleDragging(float delta)
    {
        if (_moving) return;

        if (_clearing) return;

        if (_dragging)
        {
            // Lerp to mouse position
            GlobalPosition = GlobalPosition.LinearInterpolate(GetGlobalMousePosition(), 10 * delta);
            return;
        }

        if (!_placed)
        {
            return;
        }

        GlobalPosition = GlobalPosition.LinearInterpolate(_restPoint, 10 * delta);
    }

    private async void ScaleUpDice()
    {
        Tween tween = GetNode<Tween>("Tween");
        tween.InterpolateProperty(this, "scale", Scale, new Vector2(1, 1) * 1.5f, 0.2f, Tween.TransitionType.Cubic,
            Tween.EaseType.Out);
        tween.Start();
        await ToSignal(tween, "tween_completed");
    }

    private async void ScaleDownDice()
    {
        Tween tween = GetNode<Tween>("Tween");
        tween.InterpolateProperty(this, "scale", Scale, new Vector2(1, 1), 0.2f, Tween.TransitionType.Cubic,
            Tween.EaseType.Out);
        tween.Start();
        await ToSignal(tween, "tween_completed");
    }

    public int GetDiceScore()
    {
        return _score;
    }

    public void SetDiceScore(int number)
    {
        if (number < 1)
        {
            // Death();
            OutOfStamina();
            return;
        }

        if (number > 6)
        {
            number = 6;
        }

        _score = number;
        UpdateSprite();
    }

    public void UpdateSprite(bool alive = true)
    {
        if (alive)
        {
            _sprite.Texture = GD.Load<Texture>("res://Assets/Dice/" + (_score) + ".png");
        }
        else
        {
            _sprite.Texture = GD.Load<Texture>("res://Assets/Dice/death.png");
        }
    }

    public void OutOfStamina()
    {
        _sfx.PlaySound("mulligan.wav");
        _loseParticles.Emitting = true;

        _restaurant = null;

        try
        {
            Modulate = new Color(1, 1, 1, 0.5f);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        _diceController.CheckForAllDelivered();
    }

    public void Death()
    {
        _dead = true;
        _sfx.PlaySound("mulligan.wav");
        _deathParticles.Emitting = true;
        _graveSprite.Visible = true;
        _bikeSprite.Visible = false;
        _sprite.Modulate = new Color(1, 1, 1, 0.5f);

        // UpdateSprite(false);
        _diceController.DiceDeath(this);
    }

    public void SetRollPosition(Vector2 rollPosition)
    {
        _rollPosition = rollPosition;
    }

    public void SetPlacePosition(bool placed = true)
    {
        if (placed)
        {
            _placed = true;
            _restPoint = _rollPosition;
        }
        else
        {
            _placed = false;
            _delivered = false;
            _restaurant = null;
            _bikeSprite.Visible = false;
            _graveSprite.Visible = false;
        }
    }

    private void HandleDragRelease()
    {
        Restaurant chosenHolder = _diceController.GetClosestHolder(GlobalPosition);

        if (chosenHolder == null)
        {
            if (_restaurant != null)
            {
                RemoveFromHolder();
            }
            return;
        }

        // If there is already a card in the holder, try to swap them.
        if (chosenHolder.IsSelected)
        {
            // If the current dice is not in a holder we cant swap.
            if (_restaurant == null)
            {
                return;
            }

            chosenHolder.Dice.MoveToHolder(_restaurant);
        }

        MoveToHolder(chosenHolder);
    }

    private void RemoveFromHolder()
    {
        _restaurant.Deselect();
        _restaurant = null;
        SetPlacePosition();

        _diceController.DiceMoved();
    }

    private void MoveToHolder(Restaurant holder)
    {
        holder.Select(this);
        _restPoint = holder.GlobalPosition;
        _restaurant = holder;

        _sfx.PlaySound("put_twinkle.wav");

        _diceController.DiceMoved();
    }

    public int GetPoints()
    {
        if (!_delivered)
        {
            return 0;
        }

        return _restaurant.Reward;
    }

    public void StartMoving()
    {
        if (!IsInPlay) return;

        _bikeSprite.Visible = true;
        _placed = false;
        _moving = true;
    }

    public void StopMoving()
    {
        _moving = false;
    }

    public bool IsInPlay => _restaurant != null && !_delivered && !_dead;
    public bool CanBeCollected => !_dead && Visible;

    public void DeliverySuccessful()
    {
        _clearing = false;

        _sfx.PlaySound("discard_twinkle.wav");
        _winParticles.Emitting = true;

        StopMoving();
        _delivered = true;

        _diceController.CheckForAllDelivered();
    }

    public async void ClearPath()
    {
        _clearing = true;
    }

    public bool IsDead()
    {
        return _dead;
    }

    public void ResetTurn(Vector2 startPosition)
    {
        GlobalPosition = startPosition;
        Modulate = new Color(1, 1, 1, 1);
        Visible = true;

        SetPlacePosition(false);
    }
}
