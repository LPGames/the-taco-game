# The Taco Game

## What is this?

A simple game made for gotm.io Jam 

https://gotm.io/jam/gotm-jam/26

Theme: "Food"

## How to play?

Deliver food to the king, but be careful, the king is hungry and he will eat you if you don't deliver the food fast enough.

Make it through the forest and deliver the food to the king. 

## Controls

Point and click.


## Credits

- Music: Jump and Run by Bart (https://opengameart.org/content/jump-and-run-tropical-mix)
- SFX: Fantasy Card Game SFX Pack byOlex Mazur (https://olexmazur.itch.io/fantasy-card-game)
- Icons: Game Icons (https://game-icons.net/)
- Dice (https://opengameart.org/content/multiple-dice-rolling)
